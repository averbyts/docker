#!/bin/bash

directory=$1
imagename=$2

# Build the image using $directory/Dockerfile
docker build --build-arg user_id=14806 \
             --build-arg group_id=2735 \
             --build-arg username=sftnight \
             --build-arg groupname=sf \
             -t gitlab-registry.cern.ch/sft/docker:$imagename $directory

# remove any previous existing container
docker rm -f $(docker ps -a -f name=lastbuild-$imagename -q)

# run the image
docker run -it -d --name lastbuild-$imagename gitlab-registry.cern.ch/sft/docker:$imagename

docker cp $(pwd)/post-build.sh lastbuild-$imagename:/post-build.sh

# exec the post build actions
docker exec -it lastbuild-$imagename /post-build.sh sftnight sf

# commit changes
docker commit lastbuild-$imagename gitlab-registry.cern.ch/sft/docker:$imagename

# upload the image
if [ $? -eq 0 ]; then
	docker push gitlab-registry.cern.ch/sft/docker:$imagename
fi
