# Building Tensorflow from sources

## Build the docker image

```
git clone https://gitlab.cern.ch/sft/docker.git
docker build -t tensorflow docker/special/tensorflow
```
## Run the image
```
docker run -it tensorflow bash --login
```
## Instructions inside the image

### Build the C-API library
```
git clone https://github.com/tensorflow/tensorflow.git
cd tensorflow/
git checkout v2.1.0

export PYTHON_BIN_PATH=/usr/bin/python3
./configure

BAZEL_LINKLIBS=-l%:libstdc++.a bazel test --config opt //tensorflow/tools/lib_package:libtensorflow_test
BAZEL_LINKLIBS=-l%:libstdc++.a bazel build --config opt //tensorflow/tools/lib_package:libtensorflow
```
Copy the resulting tarfile to EOS sources 



