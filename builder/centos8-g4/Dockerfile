FROM cern/c8-base
LABEL maintainer="project-lcg-spi-internal@cern.ch"
ENV container docker

# Prepare centos to use systemd
RUN dnf update -y \
    && dnf install -y systemd \
    && dnf clean all \
    && (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
        rm -fv /lib/systemd/system/multi-user.target.wants/*; \
        rm -fv /etc/systemd/system/*.wants/*; \
        rm -fv /lib/systemd/system/local-fs.target.wants/*; \
        rm -fv /lib/systemd/system/sockets.target.wants/*udev*; \
        rm -fv /lib/systemd/system/sockets.target.wants/*initctl*; \
        rm -fv /lib/systemd/system/basic.target.wants/*; \
        rm -fv /lib/systemd/system/anaconda.target.wants/*; \
        rm -fv /usr/lib/tmpfiles.d/systemd-nologin.conf;

# Install krb5.conf (before installing krb5-workstation)
COPY builder/krb5.conf/rhel /etc/krb5.conf

# Install base packages
COPY builder/centos8-g4/packages.txt /tmp/packages
RUN dnf install -y which
RUN dnf install -y epel-release
RUN dnf install -y dnf-plugin-ovl

RUN dnf update -y
RUN dnf install -y $(cat /tmp/packages)

RUN rm -fv /tmp/packages

# Default python is python 2
RUN alternatives --set python /usr/bin/python2

# Some generators need f77 defined
RUN ln -sf /usr/bin/gfortran /usr/bin/f77

# Install HEP_OSlibs
# COPY builder/centos8/heposlibs-7.2.11-3.el7.cern.x86_64.rpm /tmp/heposlibs.rpm
#RUN dnf install --nobest --skip-broken -y /tmp/heposlibs.rpm \
#    && dnf clean all \
#    && rm -fv /tmp/heposlibs.rpm

# Set the correct timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# Add user sftnight, group sf and some subfolders in the $HOME folder
RUN groupadd -g 2735 sf \
    && useradd -u 14806 -ms /bin/bash sftnight \
    && usermod -g sf sftnight \
    && mkdir /home/sftnight/.ssh \
    && mkdir /home/sftnight/.ccache

# Setup SSH configuration for Jenkins
COPY misc/config /home/sftnight/.ssh/config
RUN chmod 600 /home/sftnight/.ssh/config \
    && chown -R sftnight:sf /home/sftnight/.ssh


# Might be needed for Jenkins: https://github.com/maxfields2000/dockerjenkins_tutorial/issues/18
RUN rm -fv /run/nologin

# Create the build area
RUN mkdir -m 777 /workspace
WORKDIR /workspace

# Run bash as user sftnight as default command
USER sftnight
CMD ["/bin/bash"]
