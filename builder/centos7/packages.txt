attr
autoconf
automake
bc
bison
byacc
bzip2
bzip2-devel
ccache
cmake
cyrus-sasl
cyrus-sasl-devel
cyrus-sasl-plain
elfutils-devel
emacs
expat
expat-devel
flex
gcc
gcc-c++
gcc-gfortran
gdbm-devel
git
glibc-devel
glib2-devel
gmp-devel
java
jq
libcurl-devel
libffi
libffi-devel
libgsasl
libgsasl-devel
libICE-devel
libtool
libuuid-devel
libXmu-devel
libXpm-devel
libxkbcommon-devel
libxkbcommon-x11-devel
libxslt-devel
lsof
lzo-devel
man-db
mesa-libGL-devel
mesa-libGLU-devel
motif-devel
MySQL-python
nano
nss-devel
openssh-server
openssl
openssl-devel
perl-Env
python-devel
python36
python36-distro
readline
readline-devel
rpm-build
scl-utils
screen
subversion
systemtap
systemtap-runtime
tk-devel
tkinter
unzip
wget
xcb-util-devel
xcb-util-image-devel
xcb-util-keysyms-devel
xcb-util-renderutil-devel
xcb-util-wm-devel
xemacs
xerces-c
xerces-c-devel
xorg-x11-xauth
xrootd-client
xrootd-client-libs
xrootd-libs
xz-devel
yasm
