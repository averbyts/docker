Ubuntu 16.04 LTS
================

## Needed actions post-build

After running `docker build` to create the new ubuntu image, `kerberos`
has to be manually installed:

```
apt-get -y -q install krb5-user libkrb5-dev
```

Also, include from any other existing node the configuration of kerberos: `/etc/krb5.conf`

TODO: Find out a way to do so in the `Dockerfile`
