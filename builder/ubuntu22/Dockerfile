FROM ubuntu:22.04
LABEL maintainer="project-lcg-spi-internal@cern.ch"

# Suppress debconf messages for user interaction during installations
COPY misc/keyboard /etc/default/keyboard
ENV DEBIAN_FRONTEND noninteractive

# Install krb5.conf (before installing krb5-user)
COPY builder/krb5.conf/common /etc/krb5.conf

# Install native packages
COPY builder/ubuntu22/packages.txt /tmp/packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends apt-utils \
    && apt-get upgrade -y \
    && apt-get install -y $(cat /tmp/packages) \
    && rm -fv /tmp/packages \
    && locale-gen en_US.UTF-8 \
    && rm -rfv /var/lib/apt/lists/*

# Install network file transfer programs
RUN apt-get update \
    && apt-get install -y curl \
    && apt-get install -y wget \
    && rm -fv /tmp/packages \
    && rm -rfv /var/lib/apt/lists/*

# # Xrootd client from CERN debian repository
# RUN echo "deb http://storage-ci.web.cern.ch/storage-ci/debian/xrootd/ focal release" > /etc/apt/sources.list.d/xrootd.list
# RUN curl -sL http://storage-ci.web.cern.ch/storage-ci/storageci.key -o /tmp/storageci.key \
#     && APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add /tmp/storageci.key \
#     && rm -fr /tmp/storageci.key
# RUN apt-get update && apt-get install -y xrootd-client

# Set the correct timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# Make sure we do not use ccache wrapper
RUN update-alternatives --remove c++ /usr/bin/g++
RUN update-alternatives --remove cc /usr/bin/gcc

# Add user sftnight, group sf and some subfolders in the $HOME folder
RUN groupadd -g 2735 sf \
    && useradd -u 14806 -ms /bin/bash sftnight \
    && usermod -g sf sftnight \
    && mkdir /home/sftnight/.ssh

# Setup SSH configuration for Jenkins
COPY misc/config /home/sftnight/.ssh/config
RUN chmod 600 /home/sftnight/.ssh/config \
    && chown -R sftnight:sf /home/sftnight/.ssh

# Create the build area
RUN mkdir -m 777 /workspace
WORKDIR /workspace

# Run bash as user sftnight as default command
USER sftnight
CMD ["/bin/bash"]
