#!/bin/bash

set +x # No debug prints
set -e # Fail on error

function dockerrun {
    docker run --rm ${REGISTRY}/${BASENAME}/${PLATFORM}:latest bash -ce "$@"
}

haspy2=$1
testcomp=$2

echo "-------------------------------------------------------------------"
dockerrun "readlink /etc/localtime | grep "Zurich""
echo "-------------------------------------------------------------------"
dockerrun "if lsb_release > /dev/null 2>&1; then lsb_release -a; fi"
echo "-------------------------------------------------------------------"
dockerrun "cat /proc/version"
echo "-------------------------------------------------------------------"
dockerrun "cat /etc/*-release"
echo "-------------------------------------------------------------------"
if test "x$haspy2" = "xyes" ; then
   dockerrun "python2 -c \"import platform; print(platform.machine()); print(platform.linux_distribution())\""
else
   dockerrun "python3 -c \"import platform; print(platform.machine()); print(platform.linux_distribution())\""
fi

if [[ ${PLATFORM} != "utility" ]]
then
    # Run build tools check only for real builder images, not for 'utility'
    echo "-------------------------------------------------------------------"
    if test "x$testcomp" = "xyes" ; then
       dockerrun "gcc --version | grep 'gcc'"
       dockerrun "g++ --version | grep 'g++'"
       dockerrun "gfortran --version | grep 'ortran'"
       #dockerrun "ccache --version | head -n 1"
       echo "-------------------------------------------------------------------"
    else
       echo "---- Compiler test not performed ----------------------------------"
    fi
    dockerrun "echo \"$( make --version | grep 'ake' )   ($( make --version | grep 'uilt' ))\""
    dockerrun "cmake --version | grep 'version'"
    dockerrun "automake --version | grep 'automake'"
    dockerrun "autoconf --version | grep 'autoconf'"
fi
echo "-------------------------------------------------------------------"
if test "x$haspy2" = "xyes" ; then
   dockerrun "python2 --version"
fi
dockerrun "python3 --version"
dockerrun "pip3 install -q --user distro && python3 -c 'import distro; print(distro._distro._distro_release_info)'"
dockerrun "bash --version | grep 'bash'"
echo "-------------------------------------------------------------------"
dockerrun "git --version"
echo "-------------------------------------------------------------------"
dockerrun "bc --version | grep 'bc'"
dockerrun "which which"
echo "-------------------------------------------------------------------"
dockerrun "xz --version | grep 'lib'"
dockerrun "zip --version | grep 'This is'"
dockerrun "gzip --version | grep 'gzip'"
dockerrun "tar --version | grep 'tar'"
echo "-------------------------------------------------------------------"
dockerrun "openssl version"
echo "-------------------------------------------------------------------"
